// [WriteFile Name=CatchTheFire, Category=Geometry]
// [Legal]
// Copyright 2018 Esri.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// [Legal]

import QtQuick 2.6
import QtQuick.Controls 2.2
import Esri.Samples 1.0
import Esri.Samples 1.0
import Esri.ArcGISRuntime.Toolkit.Controls 100.6
import QtQuick.Window 2.3

CATCHTHEFIRESample {
    id: rootRectangle
    clip: true
    width: 800
    height: 600
    property alias buttonDraw: buttonDraw

    // add a mapView component
    MapView {
        anchors.fill: parent
        objectName: "mapView"

        // Declare a callout
        Callout {
            id: callout
            calloutData: rootRectangle.calloutData
            accessoryButtonHidden: true
            autoAdjustWidth: true
            maxWidth: 350
            leaderPosition: leaderPositionEnum.Automatic
        }
    }

    ComboBox {
        id: comboBoxBasemap
        anchors {
            right: parent.right
            top: parent.top
            margins: 5
        }

        property int bestWidth: implicitWidth

        width: bestWidth + indicator.width + leftPadding + rightPadding

        model: ["Topographic","Streets",
            "Streets (Vector)",
            "Streets - Night (Vector)",
            "Imagery (Raster)",
            "Imagery with Labels (Raster)",
            "Imagery with Labels (Vector)",
            "Dark Gray Canvas (Vector)",
            "Light Gray Canvas (Raster)",
            "Light Gray Canvas (Vector)",
            "Navigation (Vector)",
            "OpenStreetMap (Raster)",
            "Oceans"]

        onCurrentTextChanged: {
            // Call C++ invokable function to switch the basemaps
            rootRectangle.changeBasemap(currentText);
        }

        onModelChanged: {
            var w = bestWidth;
            for (var i = 0; i < comboBoxBasemap.model.length; ++i) {
                metrics.text = comboBoxBasemap.model[i];
                w = Math.max(w, metrics.width);
            }
            bestWidth = w;
        }

        TextMetrics {
            id: metrics
            font: comboBoxBasemap.font
        }
    }

    Button {
        id: buttonDraw
        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: 5
        }
        text: qsTr("Draw/Done")
        onClicked: {
//            text: buttonDraw.down ? qsTr("Done") : qsTr("Draw")
            rootRectangle.onDraw();
            findRow.visible = true
        }
    }

    Row {
        id: findRow
        visible: false

        anchors {
            top: parent.top
            left: parent.left

            margins: 5
        }
        spacing: 5

        TextField {
            id: findText
            width: 300

            placeholderText: "Enter a area name"
            inputMethodHints: Qt.ImhNoPredictiveText
            validator: RegExpValidator{ regExp: /^[a-zA-Z ]*$/ }
            Keys.onReturnPressed: {
                // Call C++ invokable function to run the query
                rootRectangle.runQuery(findText.text);
            }
        }

        Button {
            text: "Save"
            enabled: rootRectangle.layerInitialized
            onClicked: {
                // Call C++ invokable function to run the query
                rootRectangle.runQuery(findText.text);
                findRow.visible = false
            }
        }
    }
}
