// [WriteFile Name=CatchTheFire, Category=Geometry]
// [Legal]
// Copyright 2018 Esri.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// [Legal]

#ifndef CATCHTHEFIRE_H
#define CATCHTHEFIRE_H

namespace Esri
{
namespace ArcGISRuntime
{
class Map;
class MapQuickView;
class Graphic;
class GraphicsOverlay;
class CalloutData;
class PictureMarkerSymbol;
}
}

#include <QQuickItem>
#include "Polygon.h"
#include "Polyline.h"
#include "Point.h"
#include <QMouseEvent>
#include <QGeoCoordinate>
#include <QMap>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QComboBox>
#include <QDateTime>

class CatchTheFire : public QQuickItem
{
  Q_OBJECT

public:
  explicit CatchTheFire(QQuickItem* parent = nullptr);
  ~CatchTheFire() override = default;

  void componentComplete() override;
  static void init();
  Q_INVOKABLE void onDraw();
  Q_INVOKABLE void changeBasemap(const QString& basemap);
  Q_INVOKABLE void runQuery(const QString& stateName);

public slots:
  void loadAllMarks();

signals:
  void calloutDataChanged();

private slots:
  void onMouseClicked(QMouseEvent& event);

private:
  Esri::ArcGISRuntime::Map* m_map = nullptr;
  Esri::ArcGISRuntime::MapQuickView* m_mapView = nullptr;
  Esri::ArcGISRuntime::Graphic* m_lakeSuperiorGraphic = nullptr;
  Esri::ArcGISRuntime::Graphic* m_borderGraphic = nullptr;
  Esri::ArcGISRuntime::GraphicsOverlay* m_overlay = nullptr;
  Esri::ArcGISRuntime::Graphic* m_inputGraphic = nullptr;
  Esri::ArcGISRuntime::CalloutData* m_calloutData = nullptr;

  bool sygnal_bloked = true;

private:
  bool pointBelongsToThePolygon(QVector<Esri::ArcGISRuntime::Point> latLngs, Esri::ArcGISRuntime::Point point);
  void createGraphics();
  QVector<Esri::ArcGISRuntime::Point> QGeoCoor;
  QVector<Esri::ArcGISRuntime::Point> Points;
  QVector<Esri::ArcGISRuntime::Point> QGeoCoor2;
  QVector<Esri::ArcGISRuntime::Point> Points2;
  Esri::ArcGISRuntime::Polygon createLakeSuperiorPolygon();
  Esri::ArcGISRuntime::Polyline createBorderPolyline();
  void setWidthAndHeight(Esri::ArcGISRuntime::PictureMarkerSymbol* symbol, float size);
  void addGraphic(Esri::ArcGISRuntime::Point &point, Esri::ArcGISRuntime::PictureMarkerSymbol* symbol);
  Esri::ArcGISRuntime::GraphicsOverlay* overlay = nullptr;
  Esri::ArcGISRuntime::Point point;
  std::string current_date = "";

  QJsonDocument  json;
  QJsonArray mainArray;
  QJsonArray recordsArray;
  QJsonObject  recordObject;
//  QComboBox combobox_areas;

};

#endif // CATCHTHEFIRE_H
