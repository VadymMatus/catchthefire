// [WriteFile Name=CatchTheFire, Category=Geometry]
// [Legal]
// Copyright 2018 Esri.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// [Legal]

#include "CatchTheFire.h"

#include "Map.h"
#include "MapQuickView.h"
#include "Graphic.h"
#include "GraphicsOverlay.h"
#include "GeometryEngine.h"
#include "SpatialReference.h"
#include "PolylineBuilder.h"
#include "PolygonBuilder.h"
#include "SimpleLineSymbol.h"
#include "SimpleFillSymbol.h"
#include "CalloutData.h"
#include "SpatialReference.h"
#include "PolylineBuilder.h"
#include "Polyline.h"
#include "PolygonBuilder.h"
#include "Polygon.h"
#include "Graphic.h"
#include "GraphicsOverlay.h"
#include "SimpleMarkerSymbol.h"
#include "SimpleLineSymbol.h"
#include "SimpleFillSymbol.h"
#include "SimpleRenderer.h"
#include <QFile>
#include <QStringList>
#include <QDebug>
#include <QList>
#include "GraphicsOverlay.h"
#include "SpatialReference.h"
#include "PictureMarkerSymbol.h"
#include "KmlLayer.h"
#include "KmlDataset.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QTimer>
#include <QDate>

using namespace Esri::ArcGISRuntime;

CatchTheFire::CatchTheFire(QQuickItem* parent /* = nullptr */):
  QQuickItem(parent)
{
    QString filename = "MarksDatas.json";
    QFile file(filename);

    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        json = QJsonDocument::fromJson(stream.readAll().toUtf8());
        mainArray = json.array();
        qDebug() << mainArray;
    }

    file.close();
    qDebug() << "Количество = " << mainArray.size() << "\n";

    QTimer::singleShot(2000, this, SLOT(loadAllMarks()));
    loadAllMarks();
}

void CatchTheFire::loadAllMarks(){

//    current_date = QDate::currentDate().toString("yyyy-MM-dd");

    QDateTime now;

    QFile file(":/Samples/Geometry/CatchTheFire/MODIS_C6_Global_24h.csv");
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << file.errorString();
//        return 1;
    }

    QStringList wordList;
    file.readLine();

    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        QList<QByteArray> qList = line.split(',');

        if (current_date == "") {
            current_date = qList.at(5).toStdString();
            now = QDateTime::fromString(qList.at(5), "yyyy-dd-MM");
        }

        QDateTime nearest = QDateTime::fromString(qList.at(5), "yyyy-dd-MM");

        if (now.secsTo(nearest)) {
            for (int i = 0; i < mainArray.size(); i++) {
                QJsonObject obj = mainArray.at(i).toObject();
                QJsonArray arr = obj.value(obj.keys().last()).toArray();
    
                for (int j = 0; j < arr.size(); j++) {
                   QJsonArray gtmp = arr.at(j).toArray();
                   QGeoCoor2.push_back(Point(gtmp.first().toDouble(), gtmp.last().toDouble()));
    
    //               QJsonArray ptmp = arr.at(j).toArray();
    //               Points.push_back(Point(ptmp.first().toDouble(), ptmp.last().toDouble()));
                }
    
                if (pointBelongsToThePolygon(QGeoCoor2, Point(qList.at(0).toDouble(), qList.at(1).toDouble()))) {
                    qDebug() << "NOTIFICATION!!!";
                }
    
                QGeoCoor2.clear();
    //            Point2s.clear();
            }
        }
    }

    QTimer::singleShot(2000, this, SLOT(loadAllMarks()));
}

bool CatchTheFire::pointBelongsToThePolygon(QVector<Point> latLngs, Point point) {
        bool isBelongs = false;
        for (int i = 0, j = latLngs.size() - 1; i < latLngs.size(); j = i++)
            if (((latLngs.at(i).x() < latLngs.at(j).y()) && (latLngs.at(i).y() <= point.y()) && (point.y() <= latLngs.at(j).y()) &&
                    ((latLngs.at(j).y() - latLngs.at(i).y()) * (point.x() - latLngs.at(i).x()) > (latLngs.at(j).x() - latLngs.at(i).x()) * (point.y() - latLngs.at(i).y()))) ||
                    ((latLngs.at(i).y() > latLngs.at(j).y()) && (latLngs.at(j).y() <= point.y()) && (point.y() <= latLngs.at(i).y()) &&
                            ((latLngs.at(j).y() - latLngs.at(i).y()) * (point.x() - latLngs.at(i).x()) < (latLngs.at(j).x() - latLngs.at(i).x()) * (point.y() - latLngs.at(i).y()))))
                isBelongs = !isBelongs;

        return isBelongs;
}

void CatchTheFire::runQuery(const QString& stateName)
{
    for (int i = 0; i < QGeoCoor.size(); i++) {
       QJsonArray gtmp;
       gtmp.append(QGeoCoor.at(i).x());
       gtmp.append(QGeoCoor.at(i).y());

       QJsonArray ptmp;
       ptmp.append(Points.at(i).x());
       ptmp.append(Points.at(i).y());

       recordsArray.append(gtmp);
       recordsArray.append(ptmp);
    }

    recordObject.insert(stateName,recordsArray);
    mainArray.append(recordObject);
    json.setArray(mainArray);

    QString filename = "MarksDatas.json";
    QFile file(filename);

    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream << json.toJson() << endl;
        qDebug() << json.toJson();
    }

    file.close();


    // create a query parameter object and set the where clause
//    QueryParameters queryParams;
//    queryParams.setWhereClause(QString("STATE_NAME LIKE '" + formatStateNameForQuery(stateName) + "%'"));
//    m_featureTable->queryFeatures(queryParams);

    m_mapView->graphicsOverlays()->clear();
    QGeoCoor.clear();
    Points.clear();
}

void CatchTheFire::addGraphic(Point &point, PictureMarkerSymbol* symbol)
{
  // create graphic
  Graphic* graphic = new Graphic(point, symbol, this);
  // append to graphicsoverlay
  overlay->graphics()->append(graphic);
}

void CatchTheFire::setWidthAndHeight(PictureMarkerSymbol* symbol, float size)
{
  symbol->setWidth(size);
  symbol->setHeight(size);
}

void CatchTheFire::init()
{
  // Register the map view for QML
  qmlRegisterType<MapQuickView>("Esri.Samples", 1, 0, "MapView");
  qmlRegisterType<CatchTheFire>("Esri.Samples", 1, 0, "CATCHTHEFIRESample");
  qmlRegisterUncreatableType<CalloutData>("Esri.Samples", 1, 0, "CalloutData", "CalloutData is an uncreatable type");

}

void CatchTheFire::componentComplete()
{
  QQuickItem::componentComplete();

  // find QML MapView component
  m_mapView = findChild<MapQuickView*>("mapView");

  // connect to mouse clicked signal
  connect(m_mapView, &MapQuickView::mouseClicked, this, &CatchTheFire::onMouseClicked);
  m_mapView->blockSignals(this->sygnal_bloked);

  // Create a map using the topographic basemap
  m_map = new Map(Basemap::topographic(this), this);

  // Set initial viewpoint to Minneapolis
//  Viewpoint initialViewpoint(Envelope(-10995912.335747, 5267868.874421, -9880363.974046, 5960699.183877, SpatialReference::webMercator()));
//  m_map->setInitialViewpoint(initialViewpoint);

  // Add a GraphicsOverlay
  overlay = new GraphicsOverlay(this);
  m_mapView->graphicsOverlays()->append(overlay);

  // Add a Point Graphic
  SimpleMarkerSymbol* sms = new SimpleMarkerSymbol(SimpleMarkerSymbolStyle::Circle, QColor("red"), 5.0f /*size*/, this);
  m_inputGraphic = new Graphic(this);
  m_inputGraphic->setSymbol(sms);
  overlay->graphics()->append(m_inputGraphic);

  // Create the Dataset from an Online URL https://firms.modaps.eosdis.nasa.gov/data/active_fire/c6/kml/MODIS_C6_Global_24h.kml
  KmlDataset *m_kmlDataset = new KmlDataset(QUrl("/home/galava/Desktop/CatchTheFire/MODIS_C6_Global_24h.kml"), this);

  // Create the Layer
  KmlLayer *m_kmlLayer = new KmlLayer(m_kmlDataset, this);

  // Create a viewpoint
  const Point pt(-98, 39, SpatialReference::wgs84());
  constexpr int scale = 10000000;
  const Camera camera(pt, scale, 0, 0, 0);
  Viewpoint m_viewpoint = Viewpoint(pt, scale, camera);
  // Add the layer to the scene
//  addLayerToScene(m_kmlLayer);
  m_map->operationalLayers()->append(m_kmlLayer);
  m_mapView->setViewpoint(m_viewpoint);

  // Set map to map view
  m_mapView->setMap(m_map);
//  this->loadAllMarks();
}


Q_INVOKABLE void CatchTheFire::onDraw(){
    if (this->sygnal_bloked == true) {
        m_mapView->graphicsOverlays()->clear();
        QGeoCoor.clear();
        Points.clear();
    }

    this->sygnal_bloked = !this->sygnal_bloked;
    m_mapView->blockSignals(this->sygnal_bloked);

    if (this->sygnal_bloked == true) {
        if (QGeoCoor.size() > 1) {
            QGeoCoor.push_back(Point(QGeoCoor.first()));
            Points.push_back(Point(Points.first()));

            // create line geometry
            PolygonBuilder polylineBuilder(SpatialReference::webMercator());
            // build the polyline
            polylineBuilder.addPoint(Points.last().x(), Points.last().y());
            polylineBuilder.addPoint(Points.at(Points.size() - 2).x(), Points.at(Points.size() - 2).y());
            // create a line symbol
            SimpleLineSymbol* sls = new SimpleLineSymbol(SimpleLineSymbolStyle::Solid, QColor("red"), 3, this);
            // create a line graphic
            Graphic* lineGraphic = new Graphic(polylineBuilder.toGeometry(), this);
            // create a graphic overlay to display the line graphic
            GraphicsOverlay* lineGraphicOverlay = new GraphicsOverlay(this);
            // set the renderer of the graphic overlay to be the line symbol
            lineGraphicOverlay->setRenderer(new SimpleRenderer(sls, this));
            // add the graphic to the overlay
            lineGraphicOverlay->graphics()->append(lineGraphic);
            // add the overlay to the mapview
            m_mapView->graphicsOverlays()->append(lineGraphicOverlay);
        }
    }

}

void CatchTheFire::changeBasemap(const QString& basemap)
{
  if (!m_map)
  {
    return;
  }

  if (m_map->loadStatus() == LoadStatus::Loaded)
  {
    if (basemap == "Topographic")
      m_map->setBasemap(Basemap::topographic(this));
    else if (basemap == "Streets")
      m_map->setBasemap(Basemap::streets(this));
    else if (basemap == "Streets (Vector)")
      m_map->setBasemap(Basemap::streetsVector(this));
    else if (basemap == "Streets - Night (Vector)")
      m_map->setBasemap(Basemap::streetsNightVector(this));
    else if (basemap == "Imagery (Raster)")
      m_map->setBasemap(Basemap::imagery(this));
    else if (basemap == "Imagery with Labels (Raster)")
      m_map->setBasemap(Basemap::imageryWithLabels(this));
    else if (basemap == "Imagery with Labels (Vector)")
      m_map->setBasemap(Basemap::imageryWithLabelsVector(this));
    else if (basemap == "Dark Gray Canvas (Vector)")
      m_map->setBasemap(Basemap::darkGrayCanvasVector(this));
    else if (basemap == "Light Gray Canvas (Raster)")
      m_map->setBasemap(Basemap::lightGrayCanvas(this));
    else if (basemap == "Light Gray Canvas (Vector)")
      m_map->setBasemap(Basemap::lightGrayCanvasVector(this));
    else if (basemap == "Navigation (Vector)")
      m_map->setBasemap(Basemap::navigationVector(this));
    else if (basemap == "OpenStreetMap (Raster)")
      m_map->setBasemap(Basemap::openStreetMap(this));
    else if (basemap == "Oceans")
      m_map->setBasemap(Basemap::oceans(this));
  }
}

void CatchTheFire::onMouseClicked(QMouseEvent& event)
{
  // get the mouse click as a point
  Points.push_back(Point(m_mapView->screenToLocation(event.x(), event.y())));

          // show the clicked location on the map with a graphic
  m_inputGraphic->setGeometry(Points.last());

  // create the output SpatialReference by specifying a well known ID (WKID)
  const SpatialReference spatialReference(4326);

  // project the web mercator point to WGS84
  const Point projectedPoint = GeometryEngine::project(Points.last(), spatialReference);

    QGeoCoor.push_back(Point(projectedPoint.x() ,projectedPoint.y()));


  if(QGeoCoor.size() > 1){
      // create line geometry
      PolygonBuilder polylineBuilder(SpatialReference::webMercator());
      // build the polyline
      polylineBuilder.addPoint(Points.last().x(), Points.last().y());
      polylineBuilder.addPoint(Points.at(Points.size() - 2).x(), Points.at(Points.size() - 2).y());
      // create a line symbol
      SimpleLineSymbol* sls = new SimpleLineSymbol(SimpleLineSymbolStyle::Solid, QColor("red"), 3, this);
      // create a line graphic
      Graphic* lineGraphic = new Graphic(polylineBuilder.toGeometry(), this);
      // create a graphic overlay to display the line graphic
      GraphicsOverlay* lineGraphicOverlay = new GraphicsOverlay(this);
      // set the renderer of the graphic overlay to be the line symbol
      lineGraphicOverlay->setRenderer(new SimpleRenderer(sls, this));
      // add the graphic to the overlay
      lineGraphicOverlay->graphics()->append(lineGraphic);
      // add the overlay to the mapview
      m_mapView->graphicsOverlays()->append(lineGraphicOverlay);

    }

  // show the callout
//  emit calloutDataChanged();
}
